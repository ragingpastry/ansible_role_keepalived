ansible_role_keepalived
=========

This role sets up keepalived with a simple health check

Requirements
------------

No requirements

Role Variables
--------------

```
vip_address: x.x.x.x # set vip address
keepalived_node: MASTER # set node to be master or slave
keepalived_process_id: haproxy_DH # set keepalived process id
vrrp_script_name: check_haproxy # set name of health check script
vrrp_script: '"killall -0 haproxy"'
vrrp_check_interval: 2
vrrp_weight: 2
script_user: root
vrrp_interface: eth0 # set interface to bind vip to
virtual_router_id: 53
priority: 101
keepalived_packages:
  redhat:
    - keepalived
    - psmisc
```

Dependencies
------------

```
roles:
  - ansible-role-packages
```

Example Playbook
----------------

```
    - hosts: haproxy
      roles:
         -  role: ansible_role_keepalived
```

License
-------

BSD

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
